extends Node

func _ready():
	var player = AudioStreamPlayer.new()
	self.add_child(player)
	player.stream = load("res://music/Prominade.wav")
	player.volume_db = -18.506
	player.play()