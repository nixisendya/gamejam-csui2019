extends Area2D

const PLAYERNAME = "Player"

export(String) var textboxDialog = ""
export(String) var book_to_load

onready var book_sprite = get_node("Sprite")

# Called when the node enters the scene tree for the first time.
func _ready():
	#connect("body_entered",self,"_on_body_entered")
	#connect("body_exited",self,"on_body_exited")
	var pic_to_load = str("res://asset/books/" + book_to_load + ".png")
	var preload_pic = load(pic_to_load)
	#print(preload_pic)
	book_sprite.set_texture(preload_pic)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#if Input.is_action_just_pressed("ui_accept"):
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if (body.get_name() == PLAYERNAME):
			#print(body.get_name() + " interacts with " + get_name())
			var textbox = get_node("../Textbox")
			textbox.setDialog(textboxDialog)
			textbox.transitionInOut(true)

func _on_body_entered(body):
	print(body.get_name() + " entered the area")

func _on_body_exited(body):
	print(body.get_name() + " exited the area")
	$Sprite.frame = 1
	if(body.get_name() == PLAYERNAME):
		var textbox = get_node("../Textbox")
		textbox.transitionInOut(false)