extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var camera = get_node("../Player/Camera2D/")
const TRANSITION_IN_OFFSET = 150
const TRANSITION_OUT_OFFSET = 400
const TRANSITION_SPEED = 2.0
var transitionOffset = TRANSITION_OUT_OFFSET;
var lerpValue = 1.0
var transitionIn = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	lerpValue += TRANSITION_SPEED * delta
	if(lerpValue > 1.0):
		lerpValue = 1.0
		
	if (!transitionIn):
		transitionOffset = lerp(TRANSITION_IN_OFFSET, TRANSITION_OUT_OFFSET, lerpValue)
		$TextboxSprite.set_modulate(Color(1.0, 1.0, 1.0, 1.0-lerpValue))
		$DialogLabel.set_modulate(Color(1.0, 1.0, 1.0, 1.0-lerpValue))
	else:
		transitionOffset = lerp(TRANSITION_OUT_OFFSET, TRANSITION_IN_OFFSET, lerpValue)
		$TextboxSprite.set_modulate(Color(1.0, 1.0, 1.0, lerpValue))
		$DialogLabel.set_modulate(Color(1.0, 1.0, 1.0, lerpValue))
	
	position = camera.get_camera_position() + Vector2(0, 0-transitionOffset)
	
func transitionInOut(var transIn):
	if(transIn != transitionIn):
		lerpValue = 0.0
		transitionIn = transIn
		
func setDialog(var dialog):
	$DialogLabel.text = dialog