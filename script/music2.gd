extends Node

# Called when the node enters the scene tree for the first time.
func _ready():
	var player = AudioStreamPlayer.new()
	self.add_child(player)
	player.stream = load("res://music/8Bit Title Screen.ogg")
	player.volume_db = -15
	player.play()
