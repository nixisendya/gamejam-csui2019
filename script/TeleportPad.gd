extends Area2D

export(NodePath) var teleport_target = null

var player_in_pad = false

# Called when the node enters the scene tree for the first time.
func _ready():
	$PopupIndicator.hide()

# Called every frame. 'delta' is the elapsed time since the previous frame.
# warning-ignore:unused_argument
func _process(delta):
	if Input.is_action_just_pressed("ui_accept") and player_in_pad == true:
# warning-ignore:unused_variable
		var tree = get_tree().call_group("player","teleport_to", get_node(teleport_target).position)
	
func _on_TeleportPad_body_entered(body):
	$PopupIndicator.show()
	player_in_pad = true

func _on_TeleportPad_body_exited(body):
	$PopupIndicator.hide()
	player_in_pad = false
	
